import pathlib
from pyRDDLGym import RDDLEnv
from pyRDDLGym import ExampleManager
from pyRDDLGym.Core.Policies.Agents import RandomAgent

ExampleManager.RebuildExamples()
ExampleManager.ListExamples()

ENV = 'Elevators'
EnvInfo = ExampleManager.GetEnvInfo(ENV)

myEnv = RDDLEnv.RDDLEnv(domain=EnvInfo.get_domain(), instance=EnvInfo.get_instance(0), log=True,
                        simlogname=pathlib.Path.home()/'rddl_logs')

EnvInfo.list_instances()

myEnv.set_visualizer(EnvInfo.get_visualizer())

agent = RandomAgent(action_space=myEnv.action_space, num_actions=myEnv.numConcurrentActions)

total_reward = 0
state = myEnv.reset()
myEnv.render()

# Now we have two alternatives to run the game:
# 1 - explicitly run the interaction loop
# 2 - use the convenience 'evaluate' function

# Solution 1: explicit interaction loop
# Explicitly run the interaction loop
# for _ in range(myEnv.horizon):
#       myEnv.render()
#       next_state, reward, done, info = myEnv.step(agent.sample_action())
#       total_reward += reward
#       state = next_state
#       if done:
#             break

# Solution 2: 'evaluate' function
total_reward_x = agent.evaluate(myEnv, episodes=5, render=True)
total_reward = total_reward_x['mean']

print(total_reward_x)
print(total_reward)
myEnv.close()


# Show the current state
myEnv.state 

# Show the observation space (the list of possible things the agent can observe)
myEnv.observation_space 

# Show the list of possible actions
myEnv.action_space

# Show the constants of the environment
myEnv.non_fluents
