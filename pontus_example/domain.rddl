
domain simple_compromise {

	types {
		host: object;
        credentials: object;
	};

    pvariables {

        // Associations
        CONNECTED(host, host) : { non-fluent, bool, default = false };
        ACCESSES(credentials, host)  : { non-fluent, bool, default = false };
        STORES(host, credentials)  : { non-fluent, bool, default = false };

        // State fluents
        compromised(host) : { state-fluent, bool, default = false };
        cracked(credentials) : { state-fluent, bool, default = false };

        // Attacker action fluents
        compromise_attack(host) : { action-fluent, bool, default = false };
        crack_attack(credentials) : { action-fluent, bool, default = false };

        // Defender action fluents
        rotate_defend(credentials) : { action-fluent, bool, default = false };

        // Initial TTCs
        ittc_crack_attack(credentials) : { non-fluent, int, default = 0 };

        // Remaining TTCs
        rttc_crack_attack(credentials) : { state-fluent, int, default = 0 };

    };

cpfs {
    compromised'(?ht) =
        if (~compromised(?ht) ^ exists_{?hs : host, ?c : credentials} [CONNECTED(?hs, ?ht) ^ compromised(?hs) ^ ACCESSES(?c, ?ht) ^ cracked(?c) ^ compromise_attack(?ht)])
          then KronDelta(true)
        else if (compromised(?ht) ^ exists_{?c : credentials} [ACCESSES(?c, ?ht) ^ cracked(?c) ^ rotate_defend(?c)])
          then KronDelta(false)
        else compromised(?ht);

    cracked'(?c) =
        if (~cracked(?c) ^ crack_attack(?c) ^ rttc_crack_attack(?c) < 1 ^ exists_{?h : host} [STORES(?h, ?c) ^ compromised(?h)])
          then KronDelta(true)
        else if (cracked(?c) ^ rotate_defend(?c))
          then KronDelta(false)
        else cracked(?c);

    rttc_crack_attack'(?c) =
        if (~cracked(?c) ^ crack_attack(?c) ^ rttc_crack_attack(?c) > 0 ^ exists_{?h : host} [STORES(?h, ?c) ^ compromised(?h)])
          then (rttc_crack_attack(?c) - 1)
        else if (rotate_defend(?c))
          then (ittc_crack_attack(?c))
        else rttc_crack_attack(?c);

};


    reward = (sum_{?h: host} [compromised(?h)]);

}
